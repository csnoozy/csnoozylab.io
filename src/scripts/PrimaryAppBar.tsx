import * as React from 'react';
import {Component} from "react";
import {AppBar, Toolbar, Typography, withStyles} from "@material-ui/core";


const styles = {
    root: {
        flexGrow: 1,
        paddingBottom: '16px'
    },
};

class PrimaryAppBar extends Component<{classes: any},{}> {
    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <AppBar position='static' color='default'>
                    <Toolbar>
                        <Typography variant='title' color='inherit'> Caleb Snoozy</Typography>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}

export default withStyles(styles)(PrimaryAppBar);