import * as PIXI from "pixi.js";
import Application = PIXI.Application;
import Rectangle = PIXI.Rectangle;
import Sprite = PIXI.Sprite;
import Point = PIXI.Point;
import Container = PIXI.Container;

class FallingSprite {
    sprite: Sprite;
    direction: number = 0;
    speed: number = 0.5;
    originalTint: number = 0;

    constructor(sprite: Sprite) {
        this.sprite = sprite;
    }

    fadeOut = () => {
        if (this.sprite.tint > 0x000000) {
            this.sprite.tint -= this.sprite.tint * (Math.random() * 0.1);
        }
    };

    fadeIn = () => {
        this.sprite.tint = this.originalTint;
    }
}

export default class BackgroundAnimation {

    app: Application;
    // sprites: ParticleContainer = new ParticleContainer(10000, {
    //     scale: true,
    //     position: true,
    //     rotation: true,
    //     uvs: true,
    //     alpha: true
    // });
    sprites: Container = new Container();
    chromes: Array<FallingSprite> = [];
    dudeBoundsPadding: number;
    dudeBounds: Rectangle;
    tick: number = 0;
    images: Array<string>;
    point: Point;

    constructor(width: number, height: number, images: Array<string>) {
        this.app = new Application(width, height);
        this.app.renderer.resize(window.innerWidth, window.innerHeight);
        this.images = images;
        window.onresize = this.resize;

        document.body.appendChild(this.app.view);

        this.app.stage.addChild(this.sprites);

        this.dudeBoundsPadding = 100;
        this.dudeBounds = new Rectangle();

        this.point = new Point(0, 0);
        this.sprites.interactive = true;
        this.sprites.on('mousemove', (event: any) => {
            // console.log(this.point.x, this.point.y);
            this.point = event.data.getLocalPosition(this.sprites)
        });

        this.loadParticles();

        this.app.ticker.add(this.update);
    }

    update = () => {
        this.chromes.forEach((dude: FallingSprite, index: number) => {
            dude.sprite.x += Math.sin(dude.direction) * (dude.speed * dude.sprite.scale.y);
            dude.sprite.y += Math.cos(dude.direction) * (dude.speed * dude.sprite.scale.y);

            if ((dude.sprite.x + (dude.sprite.width / 2)) - this.point.x < 68 &&
                (dude.sprite.x + (dude.sprite.width / 2)) - this.point.x > -68 &&
                (dude.sprite.y + (dude.sprite.height / 2)) - this.point.y < 68 &&
                (dude.sprite.y + (dude.sprite.height / 2)) - this.point.y > -68
            ) {
                dude.fadeIn();
            } else {
                dude.fadeOut();
            }

            if (dude.sprite.y < this.dudeBounds.y) {
                dude.sprite.y += this.dudeBounds.height;
            } else if (dude.sprite.y > this.dudeBounds.y + this.dudeBounds.height) {
                dude.sprite.y -= this.dudeBounds.height;
            }

        });
        this.tick += 0.1;
    };

    loadParticles = () => {
        this.sprites.removeChildren(0, this.sprites.children.length);
        this.chromes = [];
        let offset = 0;
        let yCount = 0;
        for (let y = 0; y < this.app.screen.height; y += 46) {
            for (let x = -32; x < this.app.screen.width + 32; x += 53) {
                let fallingSprite = new FallingSprite(Sprite.fromImage(this.images[Math.floor(Math.random() * 5)]));
                let sprite = fallingSprite.sprite;
                sprite.x = x + offset;
                sprite.y = y;
                sprite.tint = Math.random() * 0x666;
                fallingSprite.originalTint = sprite.tint;
                sprite.tint = 0x000;

                this.chromes.push(fallingSprite);
                this.sprites.addChild(sprite);
            }
            if (offset == 0)
                offset = 53 / 2;
            else
                offset = 0;
            yCount++;
        }

        this.dudeBoundsPadding = 0;
        this.dudeBounds = new Rectangle(
            -this.dudeBoundsPadding,
            -this.dudeBoundsPadding,
            46 * yCount,
            46 * yCount
        );
    };

    resize = (event: Event) => {
        let target = event.target as Window;
        this.app.renderer.resize(target.innerWidth, target.innerHeight);
        this.loadParticles();
    }
}