import * as React from 'react';
import {Component} from 'react';
import 'typeface-roboto';
import {Grid, List, ListItem, ListItemText, PropTypes, Button} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline/CssBaseline";
import {action, observable} from "mobx";
import {inject, observer, Provider} from "mobx-react";
import {render} from "react-dom";
import PrimaryAppBar from "./PrimaryAppBar";

class Theme {
    background: string = 'background';
    foreground: PropTypes.Color = 'primary';

    constructor(bg: string, fg: PropTypes.Color) {
        this.background = bg;
        this.foreground = fg;
    }
}

const THEMES = {
    dark: new Theme('dark', 'secondary'),
    light: new Theme('secondary', 'primary')
};

class AppState {
    @observable theme: Theme = THEMES.dark;
    @observable log: Array<String> = ['This is the first element!'];

    @action toggleTheme = () => {
        this.theme = this.theme === THEMES.dark ? THEMES.light : THEMES.dark;
        this.log = ['This is the ' + this.log.length + ' element!', ...this.log];
    };
}

@observer
class App extends Component <{ appState: AppState }, {}> {
    render() {
        const {theme, toggleTheme} = this.props.appState;
        return (
            <Provider appState={this.props.appState}>
                <React.Fragment>
                    <CssBaseline/>
                    <PrimaryAppBar/>
                </React.Fragment>
            </Provider>
        );
    }
}

// @inject('appState')
// @observer
// class TestList extends Component<{ appState?: AppState }, {}> {
//     render() {
//         const {log} = this.props.appState!;
//         return (
//             <React.Fragment>
//                 {log.map((value, index) => <Grid item key={index}><ListItemText primary={value}/></Grid>)}
//             </React.Fragment>
//         );
//     }
// }

render(<App appState={new AppState()}/>, document.getElementById('app'));